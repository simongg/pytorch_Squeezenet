import torch
import torch.optim as optim
from torch.autograd import Variable
import argparse
import numpy as np
import sigmoid_squeezenet
import original_squeezenet as og_squeezenet
import datanet_loader
import torch.nn.functional as F
import matplotlib.pyplot as plt
# plt.switch_backend('agg')
from collections import defaultdict

parser = argparse.ArgumentParser('Options for training SqueezeNet in pytorch')
parser.add_argument('--batch-size', type=int, default=64, metavar='N',
                    help='batch size of train')

parser.add_argument('--epoch', type=int, default=80, metavar='N',
                    help='number of epochs to train for')

parser.add_argument('--learning-rate', type=float, default=0.001, metavar='LR',
                    help='learning rate')

parser.add_argument('--momentum', type=float, default=0.9, metavar='M',
                    help='percentage of past parameters to store')

parser.add_argument('--no-cuda', action='store_true', default=False,
                    help='use cuda for training')

parser.add_argument('--log-schedule', type=int, default=64, metavar='N',
                    help='number of epochs to save snapshot after')

parser.add_argument('--seed', type=int, default=1,
                    help='set seed to some constant value to reproduce '
                    'experiments')

parser.add_argument('--pretrained', type=str, default=False,
                    help='Use a pretrained model')

parser.add_argument('--want_to_test', type=bool,
                    default=False, help='make true if you just want to test')

parser.add_argument('--epoch_55', action='store_true', default=True,
                    help='would you like to use 55 epoch learning rule')

parser.add_argument('--num_classes', type=int, default=10,
                    help="how many classes training for")

parser.add_argument('--net-type', type=str, default='OG',
                    help='squeezenet or alexnet')

parser.add_argument('--unique', type=str, default='testing',
                    help='Make unique plots')

parser.add_argument('--use-pct', type=bool, default=False,
                    help='Use pct or standard?')

parser.add_argument('--sweep', type=str, default='None',
                    help='Define what to sweep (sr or pct3)')


args = parser.parse_args()

args.cuda = not args.no_cuda and torch.cuda.is_available() # and False

print(args.cuda)

torch.manual_seed(args.seed)
if args.cuda:
    torch.cuda.manual_seed(args.seed)

# Load net and data loaders
_, train_loader, test_loader = datanet_loader.datanet(args)
best_accuracy = 0.0
loss_lst = []
fig1, ax1 = plt.subplots()


# create optimizer
# using the 55 epoch learning rule here
def paramsforepoch(epoch):
    p = dict()
    regimes = [[1, 18, 5e-3, 5e-4],
               [19, 29, 1e-3, 5e-4],
               [30, 43, 5e-4, 5e-4],
               [44, 52, 1e-4, 0],
               [53, 1e8, 1e-5, 0]]

    for i, row in enumerate(regimes):
        if epoch >= row[0] and epoch <= row[1]:
            p['learning_rate'] = row[2]
            p['weight_decay'] = row[3]
    return p


def view_image(loader):
    dataiter = iter(loader)
    images, labels = dataiter.next()

    rint = np.random.randint(len(images))
    im = np.array(images[rint]).swapaxes(0, 2)
    img = (im - np.min(im))/np.max((im - np.min(im)))

    plt.figure(num=None, figsize=(8, 8), dpi=80)
    plt.rcParams.update({'font.size': 24})
    plt.imshow(img)
    plt.title('Original Instance\nTarget: {}'.format(labels[rint].item()))
    plt.xlabel('Pixel')
    plt.ylabel('Pixel')
    plt.show()


def adjustlrwd(params, optimizer):
    for param_group in optimizer.state_dict()['param_groups']:
        param_group['lr'] = params['learning_rate']
        param_group['weight_decay'] = params['weight_decay']


# train the network
def train(epoch, net, optimizer, args, sr, pct3, ks):
    # set the optimizer for this epoch
    if args.epoch_55:
        params = paramsforepoch(epoch)
        print(("Configuring optimizer with lr={:.5f} and "
               "weight_decay={:.4f}".format(params['learning_rate'],
                                            params['weight_decay'])))
        adjustlrwd(params, optimizer)
    ###########################################################################
    global loss_lst

    avg_loss = []

    correct = 0
    score_len = 0
    net.train()
    for b_idx, (data, target) in enumerate(train_loader):

        if args.cuda:
            data, target = data.cuda(), target.cuda()
        # convert the data and targets into Variable and cuda form
        data, target = Variable(data), Variable(target)

        # train the network
        optimizer.zero_grad()
        output = net(data)
        loss = F.nll_loss(output.squeeze(), target)

        # compute the accuracy
        avg_loss.append(loss.item())
        loss.backward()
        optimizer.step()

        if b_idx % args.log_schedule == 0:
            loss_lst.append(np.mean(avg_loss))
            avg_loss = []
            print(('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, (b_idx+1) * len(data), len(train_loader.dataset),
                100. * (b_idx+1)*len(data) / len(train_loader.dataset),
                loss.item())))

            # also plot the loss, it should go down exponentially at some point
            ax1.cla()
            ax1.plot(loss_lst)
            ax1.set_title('Average loss of {} batch'.
                          format(args.log_schedule))
            ax1.set_xlabel('Logged Batches (every {})'.
                           format(args.log_schedule))
            ax1.set_ylabel('Loss')
            fig1.savefig('Figures/avg_loss_{}_numclasses={}_{}_sr='
                         '{:.3f}_pct3={:.3f}_ks={}.jpg'.
                         format(args.batch_size, args.num_classes, args.unique,
                                sr, pct3, ks))

        # get the index of the max log-probability
        pred = output.data.max(1)[1]
        correct += pred.eq(target.view_as(pred)).sum().item()
        score_len += len(target)
        trn_accuracy = correct / score_len
    return trn_accuracy


def test():
    acc = defaultdict(list)
    sweeps = ['none']
    num_classes = 10
    for sweep in sweeps:
        pct3s = [0.5]
        srs = [0.125]

        for sr in srs:
            for pct3 in pct3s:

                net = og_squeezenet.SqueezeNet(num_classes=num_classes,
                                               pct3=pct3, sr=sr)
                # load the best saved model
                filename = ('OG_bs=64_numclasses10_testing_sr={:.3f}'
                            '_pct3={:.3f}.pth'.format(sr, pct3))
                print('testing {}'.format(filename))

                weights = torch.load('models/{}'.format(filename),
                                     map_location='cpu')
                net.load_state_dict(weights)

                if args.cuda:
                    net.cuda()

                test_correct = 0
                total_examples = 0
                net.eval()
                for idx, (data, target) in enumerate(test_loader):
                    if idx < 73:
                        continue
                    data, target = Variable(data), Variable(target)
                    if args.cuda:
                        data, target = data.cuda(), target.cuda()

                    # do the forward pass
                    score = net.forward(data)
                    pred = score.data.max(1)[1]  # got the indices of the max,
                    # match them
                    test_correct += pred.eq(target.view_as(pred)).sum().item()
                    total_examples += len(target)

                print(("Predicted {} out of {} correctly".format(test_correct,
                       total_examples)))
                print('Accuracy:  {:.3f}'.format(test_correct/total_examples))

                acc[sweep].append(100.0 * test_correct / (float(
                        total_examples)))

#    plt.figure(num=None, figsize=(15, 10), dpi=80)
#    plt.rcParams.update({'font.size': 24})
#    plt.plot(srs_plot, acc['sr'])
#    plt.scatter(srs_plot, acc['sr'])
#    plt.title('Accuracy vs Squeeze Ratio')
#    plt.ylabel('Accuracy')
#    plt.xlabel('Squeeze Ratio')
#    plt.ylim([0, 1])
#    plt.xticks(srs_plot)
#    plt.grid()
#    plt.savefig('Figures/Acc_vs_Sr.pdf')
#
#    plt.figure(num=None, figsize=(15, 10), dpi=80)
#    plt.rcParams.update({'font.size': 24})
#    plt.plot(pct3s_plot, acc['pct3'])
#    plt.scatter(pct3s_plot, acc['pct3'])
#    plt.title('Accuracy vs percentage of 3x3 kernels in Expand Layers')
#    plt.ylabel('Accuracy')
#    plt.xlabel('Percentage of 3x3 Kernels')
#    plt.ylim([0, 1])
#    plt.xticks(pct3s_plot)
#    plt.grid()
#    plt.savefig('Figures/Acc_vs_Pct3.pdf')


def val(net, args, sr, pct3, ks):
    global best_accuracy
    correct = 0
    score_len = 0
    net.eval()
    for idx, (data, target) in enumerate(test_loader):
        if idx == 73:
            break

        if args.cuda:
            data, target = data.cuda(), target.cuda()
        data, target = Variable(data), Variable(target)

        # do the forward pass
        score = net.forward(data)
        pred = score.data.max(1)[1]  # got the indices of the maximum, match
        # them
        correct += pred.eq(target.view_as(pred)).sum().item()
        score_len += len(target)

    print(("predicted {} out of {}".format(correct, score_len)))
    val_accuracy = correct / score_len
    print(("accuracy = {:.2f}".format(val_accuracy)))

    # now save the model if it has better accuracy than the best model
    # seen so forward
    if val_accuracy > best_accuracy:
        best_accuracy = val_accuracy
        # save the model
        torch.save(net.state_dict(),
                   'models/{}_{}_numclasses{}_{}_'
                   'sr={:.3f}_pct3={:.2f}.pth'
                   .format(args.net_type, ks,
                           args.num_classes, args.unique, sr, pct3))
    return val_accuracy


if __name__ == '__main__' and 1:
    activation = 'ReLU'
    if not args.want_to_test:
        for sweep in ['none']:
            if sweep == 'sr':
                srs = [0.125, 0.25, 0.5, 0.75, 1.0]
                pct3s = [0.5]
            elif sweep == 'pct3':
                pct3s = [0.5, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875,
                         0.99]

                srs = [0.125]
            else:
                pct3s = [0.5]
                srs = [0.125]

            kern_sz = [3]
            for ks in kern_sz:
                final_val_acc = []

                for pct3 in pct3s:
                    for sr in srs:
                        if activation == 'sigmoid':
                            net = sigmoid_squeezenet.SqueezeNet()
                        else:
                            net = og_squeezenet.SqueezeNet(
                                    num_classes=args.num_classes, sr=sr,
                                    pct3=pct3, kernel_size=ks)

                        if args.cuda:
                            net.cuda()

                        # create a temporary optimizer
                        optimizer = optim.SGD(net.parameters(),
                                              lr=args.learning_rate,
                                              momentum=0.9, weight_decay=5e-4)
                        print(sr, pct3)
#                        print(net)

                        # Define a bunch of global vars (I dont like this)
                        best_accuracy = 0.0
                        loss_lst = []
                        plt.close('all')
                        fig1, ax1 = plt.subplots()
                        fig2, ax2 = plt.subplots()
                        train_acc, val_acc = list(), list()

                        for ii in range(1, args.epoch+1):
                            train_acc.append(train(ii, net, optimizer, args,
                                                   sr, pct3, ks))
                            val_acc.append(val(net, args, sr, pct3, ks))

                            ax2.cla()
                            ax2.plot(train_acc, 'g', label='Train Accuracy')
                            ax2.plot(val_acc, 'b',  label='Valid Accuracy')
                            ax2.set_title('Validation and Train Accuracy '
                                          'per epoch')
                            ax2.legend()
                            ax2.set_xlabel('Epoch'.format(args.log_schedule))
                            ax2.set_ylabel('Accuracy')
                            fig2.savefig('Figures/{}_bs_{}_numclasses'
                                         '={}_{}_sr={:.3f}_pct3={:.3f}.jpg'.
                                         format(args.net_type, args.batch_size,
                                                args.num_classes, args.unique,
                                                sr, pct3))
                        final_val_acc.append(best_accuracy)
