# -*- coding: utf-8 -*-
"""
Created on Mon Apr  1 12:35:06 2019

@author: sgeoff1
"""

import model
import torch
import torchvision.datasets as datasets
import torchvision.transforms as transforms

def datanet(args):
    kwargs = {'num_workers': 1, 'pin_memory': True} if args.cuda else {}
    if args.net_type == 'squeezenet':
        #    net = models.squeezenet1_1(pretrained=args.pretrained)
        net = model.squeezenet(args.num_classes)
        if args.num_classes == 100:
            train_loader = torch.utils.data.DataLoader(
                datasets.CIFAR100('../cifar10/', train=True, download=True,
                                 transform=transforms.Compose([
                                   transforms.RandomHorizontalFlip(),
                                   transforms.ToTensor(),
                                   transforms.Normalize((0.491399689874,
                                                         0.482158419622,
                                                         0.446530924224),
                                                        (0.247032237587,
                                                         0.243485133253,
                                                         0.261587846975))
                                                               ])),
                batch_size=args.batch_size, shuffle=True, **kwargs)

            test_loader = torch.utils.data.DataLoader(
                datasets.CIFAR100('../cifar10/', train=False,
                                  transform=transforms.Compose([
                                   transforms.RandomHorizontalFlip(),
                                   transforms.ToTensor(),
                                   transforms.Normalize((0.491399689874,
                                                         0.482158419622,
                                                         0.446530924224),
                                                        (0.247032237587,
                                                         0.243485133253,
                                                         0.261587846975))
                                                               ])),
                batch_size=args.batch_size, shuffle=True, **kwargs)
        else:
            train_loader = torch.utils.data.DataLoader(
                datasets.CIFAR10('../cifar10/', train=True, download=True,
                                 transform=transforms.Compose([
                                   transforms.RandomHorizontalFlip(),
                                   transforms.Resize(224),
                                   transforms.ToTensor(),
                                   transforms.Normalize((0.491399689874,
                                                         0.482158419622,
                                                         0.446530924224),
                                                        (0.247032237587,
                                                         0.243485133253,
                                                         0.261587846975))
                                                               ])),
                batch_size=args.batch_size, shuffle=True, **kwargs)

            test_loader = torch.utils.data.DataLoader(
                datasets.CIFAR10('../cifar10/', train=False,
                                 transform=transforms.Compose([
#                                   transforms.RandomHorizontalFlip(),
                                   transforms.Resize(224),
                                   transforms.ToTensor(),
                                   transforms.Normalize((0.491399689874,
                                                         0.482158419622,
                                                         0.446530924224),
                                                        (0.247032237587,
                                                         0.243485133253,
                                                         0.261587846975))
                                                               ])),
                batch_size=args.batch_size, shuffle=True, **kwargs)

    else:
        net = model.alexnet(num_classes=args.num_classes)

        if args.num_classes == 100:
            train_loader = torch.utils.data.DataLoader(
                datasets.CIFAR100('../cifar10/', train=True, download=True,
                                  transform=transforms.Compose([
                                   transforms.RandomHorizontalFlip(),
                                   transforms.Resize(224),
                                   transforms.ToTensor(),
                                   transforms.Normalize((0.491399689874,
                                                         0.482158419622,
                                                         0.446530924224),
                                                        (0.247032237587,
                                                         0.243485133253,
                                                         0.261587846975))
                                                               ])),
                batch_size=args.batch_size, shuffle=True, **kwargs)

            test_loader = torch.utils.data.DataLoader(
                datasets.CIFAR100('../cifar10/', train=False,
                                 transform=transforms.Compose([
                                   transforms.RandomHorizontalFlip(),
                                   transforms.Resize(224),
                                   transforms.ToTensor(),
                                   transforms.Normalize((0.491399689874,
                                                         0.482158419622,
                                                         0.446530924224),
                                                        (0.247032237587,
                                                         0.243485133253,
                                                         0.261587846975))
            ])),
                batch_size=args.batch_size, shuffle=True, **kwargs)
        else:
            train_loader = torch.utils.data.DataLoader(
                datasets.CIFAR10('../cifar10/', train=True, download=True,
                                 transform=transforms.Compose([
                                   transforms.RandomHorizontalFlip(),
                                   transforms.Resize(224),
                                   transforms.ToTensor(),
                                   transforms.Normalize((0.491399689874,
                                                         0.482158419622,
                                                         0.446530924224),
                                                        (0.247032237587,
                                                         0.243485133253,
                                                         0.261587846975))
                                                               ])),
                batch_size=args.batch_size, shuffle=True, **kwargs)

            test_loader = torch.utils.data.DataLoader(
                datasets.CIFAR10('../cifar10/', train=False,
                                 transform=transforms.Compose([
                                   transforms.RandomHorizontalFlip(),
                                   transforms.Resize(224),
                                   transforms.ToTensor(),
                                   transforms.Normalize((0.491399689874,
                                                         0.482158419622,
                                                         0.446530924224),
                                                        (0.247032237587,
                                                         0.243485133253,
                                                         0.261587846975))
            ])),
                batch_size=args.batch_size, shuffle=True, **kwargs)
    return net, train_loader, test_loader
