### Pytorch Squeeznet

Pytorch implementation of Squeezenet model as described in https://arxiv.org/abs/1602.07360 on cifar-10 Data.

The definition of Squeezenet model is present **model.py**.
The training procedure resides in the file **main.py**

Command to train the Squeezenet model on CIFAR 10 data is:
```bash
python main.py --batch-size 32 --epoch 10
```
Other options which can be used are specified in ** *_main.py **

model.py contains all the models used in our project
param_sweep.py is, as the name says, a parameter sweeper function that sweeps through 'SR', 'PCT3', and kernel size
datanet_loader.py loads both the data (using torchvision) and the neural network
sigmoid_squeezenet.py contains a Squeezenet NN but uses sigmoid instead of ReLU
alex_net.py contains a usable version of alexnet
com_bypass_squeezenet.py contains a version of squeezenet that uses a complex bypass
sim_bypass_squeezenet.py contains a vesrion of squeezenet that uses a simple bypass

Every function that contains **main** serves as the training and testing function for various models
